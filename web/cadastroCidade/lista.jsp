<%-- 
    Document   : lista
    Created on : 24/10/2017, 18:51:19
    Author     : sala304b
--%>

<%@page import="java.util.List"%>
<%@page import="br.com.senac.modelo.Cidade"%>
<%@page import="br.com.senac.banco.CidadeDAO"%>
<%@page import="br.com.senac.banco.CidadeDAO"%>
<%
    CidadeDAO dao = new CidadeDAO();
    List<Cidade> lista = dao.listarTodos();
%>

<div class="container">
    <fieldset>
        <legend>Lista de Pa�ses</legend>

        <table class="table table-hover">
            <thead>
                <tr>
                    <th>C�digo</th>
                    <th>Pa�s</th>
                </tr>
            </thead>
            <tbody>


                <% for (Cidade p : lista) { %>

                <tr>
                    <td><% out.print(p.getCodigo()); %></td>
                    <td><% out.print(p.getNome()); %></td>
                </tr>

                <% }%>

            </tbody>
        </table>

    </fieldset>

</div>

<jsp:include page="../footer.jsp" />












